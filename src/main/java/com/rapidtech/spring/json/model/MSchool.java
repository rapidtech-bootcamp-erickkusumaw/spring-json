package com.rapidtech.spring.json.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MSchool {
    private String title;
    private String name;
    private String level;
}
