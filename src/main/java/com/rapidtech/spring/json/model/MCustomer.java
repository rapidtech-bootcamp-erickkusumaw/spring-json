package com.rapidtech.spring.json.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MCustomer {
    private Long id;
    private String fullName;
    private List<MAddress> address;
    private String gender;
    private Date dateOfBirth;
    private String placeOfBirth;
    private List<MSchool> schools;
}
