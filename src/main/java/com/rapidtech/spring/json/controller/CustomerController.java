package com.rapidtech.spring.json.controller;

import com.rapidtech.spring.json.model.MCustomer;
import com.rapidtech.spring.json.model.MCustomerReques;
import com.rapidtech.spring.json.model.ResponseModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @PostMapping
    public ResponseEntity<Object> saveCustomer(@RequestBody MCustomerReques reques){
        return ResponseEntity.ok().body(
                new ResponseModel(200, "Success", reques)
        );
    }
}
